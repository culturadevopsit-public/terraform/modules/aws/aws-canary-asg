provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

module "module_usage_howto" {
  source = "./../.."

  project = {
    project     = "my-project"
    environment = "dev"
    createdBy   = "markitos"
    group       = "my-group"
  }

  resources = {
    name                         = "markitos_asg"
    lb_access_log_enabled        = true
    lb_connection_log_enabled    = true
    lb_delete_protection_enabled = false
    asg_desired_capacity         = 2
    asg_min_size                 = 1
    asg_max_size                 = 2
    blue_ami                     = "ami-0c38b837cd80f13bb"
    green_ami                    = "ami-0c38b837cd80f13bb"
    blue_user_data_file          = "${path.cwd}/scripts/blue_user_data.sh"
    green_user_data_file         = "${path.cwd}/scripts/green_user_data.sh"
    blue_instance_type           = "t3.micro"
    green_instance_type          = "t3.micro"
    traffic_distribution         = "split"
    vpc_id                       = "vpc-0c78ce9c11b834672"
    alb_subnets_ids              = toset(["subnet-0b3fd46b4aff02ee7", "subnet-04d15ed60eafbaca7"])
    tags = {
      Name = "basic-canary-asg"
    }
  }
}
