<!-- BEGIN_TF_DOCS -->



## Resources

The following resources are used by this module:

- [aws_alb_listener.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/alb_listener) (resource)
- [aws_alb_target_group.blue](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/alb_target_group) (resource)
- [aws_alb_target_group.green](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/alb_target_group) (resource)
- [aws_autoscaling_group.asg_blue](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group) (resource)
- [aws_autoscaling_group.asg_green](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group) (resource)
- [aws_launch_template.blue](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template) (resource)
- [aws_launch_template.launch_green](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template) (resource)
- [aws_lb.alb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb) (resource)
- [aws_s3_bucket.logs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) (resource)
- [aws_s3_bucket_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) (resource)
- [aws_security_group.alb_sg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) (resource)
- [aws_security_group.instances_sg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) (resource)
- [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) (data source)
```yaml
locals {
  resources = {
    name                         = var.resources.name
    vpc_id                       = var.resources.vpc_id
    asg_min_size                 = var.resources.asg_min_size
    asg_max_size                 = var.resources.asg_max_size
    asg_desired_capacity         = var.resources.asg_desired_capacity
    blue_user_data_file          = try(var.resources.blue_user_data_file, null)
    green_user_data_file         = try(var.resources.green_user_data_file, null)
    alb_subnets_ids              = toset(var.resources.alb_subnets_ids)
    lb_logs_bucket_name          = replace("${var.resources.name}-logs", "_", "-")
    lb_access_log_enabled        = try(var.resources.lb_access_log_enabled != null ? var.resources.lb_access_log_enabled : null, null)
    lb_connection_log_enabled    = try(var.resources.lb_connection_log_enabled != null ? var.resources.lb_connection_log_enabled : null, null)
    lb_delete_protection_enabled = try(var.resources.lb_delete_protection_enabled != null ? var.resources.lb_delete_protection_enabled : null, null)
    traffic_distribution         = var.resources.traffic_distribution
    blue_ami                     = var.resources.blue_ami
    green_ami                    = var.resources.green_ami
    blue_instance_type           = var.resources.blue_instance_type
    green_instance_type          = var.resources.green_instance_type
    tags                         = try(var.resources.tags != null ? var.resources.tags : null, null)
  }

  traffic_dist_map = {
    blue = {
      blue  = 100
      green = 0
    }
    blue-90 = {
      blue  = 90
      green = 10
    }
    split = {
      blue  = 50
      green = 50
    }
    green-90 = {
      blue  = 10
      green = 90
    }
    green = {
      blue  = 0
      green = 100
    }
  }


  common_tags = {
    Project     = var.project.project
    Environment = var.project.environment
    CreatedBy   = var.project.createdBy
    Group       = var.project.group
  }
}

resource "aws_s3_bucket" "logs" {
  bucket = local.resources.lb_logs_bucket_name

  force_destroy = true
  lifecycle {
    prevent_destroy = false
  }

  tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
    Name = "${local.resources.name}-alb"
  })
}

resource "aws_s3_bucket_policy" "this" {
  bucket = aws_s3_bucket.logs.id
  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "AWS" : "arn:aws:iam::156460612806:root"
        },
        "Action" : "s3:PutObject",
        "Resource" : "arn:aws:s3:::${aws_s3_bucket.logs.id}/AWSLogs/${data.aws_caller_identity.current.account_id}/*"
      }
    ]
  })
}

data "aws_caller_identity" "current" {}


resource "aws_lb" "alb" {
  desync_mitigation_mode           = "defensive"
  drop_invalid_header_fields       = false
  enable_cross_zone_load_balancing = true
  enable_deletion_protection       = try(var.resources.lb_delete_protection_enabled, true)
  enable_http2                     = true
  enable_waf_fail_open             = false
  idle_timeout                     = 60
  internal                         = false
  ip_address_type                  = "ipv4"
  load_balancer_type               = "application"
  security_groups                  = [aws_security_group.alb_sg.id]
  subnets                          = local.resources.alb_subnets_ids

  dynamic "access_logs" {
    for_each = try(var.resources.lb_access_log_enabled, false) ? [1] : [0]
    content {
      bucket  = aws_s3_bucket.logs.id
      enabled = true
    }
  }

  dynamic "connection_logs" {
    for_each = try(var.resources.lb_connection_log_enabled, false) ? [1] : [0]
    content {
      bucket  = aws_s3_bucket.logs.id
      enabled = true
    }
  }

  tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
    Name = "${local.resources.name}-alb"
  })
}

resource "aws_alb_listener" "default" {
  load_balancer_arn = aws_lb.alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "forward"
    forward {
      target_group {
        arn    = aws_alb_target_group.blue.arn
        weight = lookup(local.traffic_dist_map[local.resources.traffic_distribution], "blue", 100)
      }

      target_group {
        arn    = aws_alb_target_group.green.arn
        weight = lookup(local.traffic_dist_map[local.resources.traffic_distribution], "green", 0)
      }

      stickiness {
        enabled  = true
        duration = 60
      }
    }
  }

  depends_on = [aws_lb.alb]
}

resource "aws_alb_target_group" "blue" {
  connection_termination             = false
  deregistration_delay               = "300"
  lambda_multi_value_headers_enabled = false
  load_balancing_algorithm_type      = "round_robin"
  port                               = 80
  protocol                           = "HTTP"
  protocol_version                   = "HTTP1"
  proxy_protocol_v2                  = false
  slow_start                         = 0
  target_type                        = "instance"
  vpc_id                             = local.resources.vpc_id

  health_check {
    enabled             = true
    healthy_threshold   = 3
    interval            = 30
    matcher             = "200-399"
    path                = "/"
    port                = "8080"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 3
  }

  stickiness {
    cookie_duration = 86400
    enabled         = true
    type            = "lb_cookie"
  }
}

resource "aws_alb_target_group" "green" {
  connection_termination             = false
  deregistration_delay               = "300"
  lambda_multi_value_headers_enabled = false
  load_balancing_algorithm_type      = "round_robin"
  port                               = 80
  protocol                           = "HTTP"
  protocol_version                   = "HTTP1"
  proxy_protocol_v2                  = false
  slow_start                         = 0
  target_type                        = "instance"
  vpc_id                             = local.resources.vpc_id

  health_check {
    enabled             = true
    healthy_threshold   = 3
    interval            = 30
    matcher             = "200-399"
    path                = "/"
    port                = "8080"
    protocol            = "HTTP"
    timeout             = 5
    unhealthy_threshold = 3
  }

  stickiness {
    cookie_duration = 86400
    enabled         = true
    type            = "lb_cookie"
  }
}


resource "aws_launch_template" "blue" {
  name_prefix            = "launch_blue-"
  image_id               = local.resources.blue_ami
  instance_type          = local.resources.blue_instance_type
  vpc_security_group_ids = [aws_security_group.instances_sg.id]
  user_data              = local.resources.blue_user_data_file != null ? base64encode(templatefile(local.resources.blue_user_data_file, {})) : null

  lifecycle {
    create_before_destroy = true
  }

  tag_specifications {
    resource_type = "instance"
    tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
      Name = "launch_blue - ALB ${local.resources.name}"
    })
  }
}

resource "aws_autoscaling_group" "asg_blue" {
  target_group_arns   = toset([aws_alb_target_group.blue.arn])
  desired_capacity    = local.resources.asg_desired_capacity
  min_size            = local.resources.asg_min_size
  max_size            = local.resources.asg_max_size
  vpc_zone_identifier = local.resources.alb_subnets_ids

  launch_template {
    id      = aws_launch_template.blue.id
    version = "$Latest"
  }

  depends_on = [aws_launch_template.blue]
}

resource "aws_launch_template" "launch_green" {
  name_prefix            = "launch_green-"
  image_id               = local.resources.green_ami
  instance_type          = local.resources.green_instance_type
  vpc_security_group_ids = [aws_security_group.instances_sg.id]
  user_data              = local.resources.green_user_data_file != null ? base64encode(templatefile(local.resources.green_user_data_file, {})) : null

  lifecycle {
    create_before_destroy = true
  }

  tag_specifications {
    resource_type = "instance"
    tags = merge(local.common_tags, local.resources.tags != null ? local.resources.tags : null, {
      Name = "launch_green - ALB ${local.resources.name}"
    })
  }
}
resource "aws_autoscaling_group" "asg_green" {
  target_group_arns   = toset([aws_alb_target_group.green.arn])
  vpc_zone_identifier = local.resources.alb_subnets_ids
  desired_capacity    = local.resources.asg_desired_capacity
  min_size            = local.resources.asg_min_size
  max_size            = local.resources.asg_max_size

  launch_template {
    id      = aws_launch_template.launch_green.id
    version = "$Latest"
  }

  depends_on = [aws_launch_template.launch_green]
}

resource "aws_security_group" "alb_sg" {
  name        = "${local.resources.name}-alb-sg"
  description = "Security group for ALB"
  vpc_id      = local.resources.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(local.common_tags, {
    Name = "${local.resources.name}-alb-sg"
  })
}

resource "aws_security_group" "instances_sg" {
  name        = "${local.resources.name}-instances-sg"
  description = "Security group for EC2 instances"
  vpc_id      = local.resources.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.alb_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(local.common_tags, {
    Name = "${local.resources.name}-instances-sg"
  })
}
```

## Providers

The following providers are used by this module:

- <a name="provider_aws"></a> [aws](#provider\_aws) (~>5.35.0)
```yaml
terraform {
  required_version = ">1.7.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>5.35.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "~>3.1.0"
    }
  }
}
```

## Required Inputs

The following input variables are required:

### <a name="input_project"></a> [project](#input\_project)

Description: some common settings like project name, environment, and tags for all resources-objects

Type:

```hcl
object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
```

### <a name="input_resources"></a> [resources](#input\_resources)

Description: resources of objects to create

Type:

```hcl
object({
    name                         = string
    blue_ami                     = string
    green_ami                    = string
    asg_desired_capacity         = number
    asg_min_size                 = number
    asg_max_size                 = number
    blue_user_data_file          = optional(string)
    green_user_data_file         = optional(string)
    lb_access_log_enabled        = optional(bool)
    lb_connection_log_enabled    = optional(bool)
    lb_delete_protection_enabled = optional(bool)
    alb_subnets_ids              = set(string)
    vpc_id                       = string
    traffic_distribution         = string
    blue_instance_type           = string
    green_instance_type          = string
    tags                         = optional(map(string))

  })
```

## Optional Inputs

No optional inputs.
```yaml
variable "resources" {
  description = "resources of objects to create"
  nullable    = false
  type = object({
    name                         = string
    blue_ami                     = string
    green_ami                    = string
    asg_desired_capacity         = number
    asg_min_size                 = number
    asg_max_size                 = number
    blue_user_data_file          = optional(string)
    green_user_data_file         = optional(string)
    lb_access_log_enabled        = optional(bool)
    lb_connection_log_enabled    = optional(bool)
    lb_delete_protection_enabled = optional(bool)
    alb_subnets_ids              = set(string)
    vpc_id                       = string
    traffic_distribution         = string
    blue_instance_type           = string
    green_instance_type          = string
    tags                         = optional(map(string))

  })
}

variable "project" {
  description = "some common settings like project name, environment, and tags for all resources-objects"
  nullable    = false
  type = object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
}
```

## Outputs

The following outputs are exported:

### <a name="output_alb_endpoint"></a> [alb\_endpoint](#output\_alb\_endpoint)

Description: Public DNS of ALB

### <a name="output_debug"></a> [debug](#output\_debug)

Description: For debug purpose.
```yaml
output "alb_endpoint" {
  description = "Public DNS of ALB"
  value       = aws_lb.alb.dns_name
}

output "debug" {
  description = "For debug purpose."
  value       = var.resources
}
```

## Example from YAML
```yaml
project:
  project: my-project
  region: eu-west-1
  profile: default
  createdBy: markitos
  environment: dev
  group: web
resources:
  name: "basic_canary_asg"
  blue_ami: "ami-xxxxx"
  green_ami: "ami-xxxxx"
  lb_access_log_enabled: true
  lb_connection_log_enabled: true
  lb_delete_protection_enabled: false
  asg_desired_capacity: 2
  asg_min_size: 1
  asg_max_size: 2
  blue_user_data_file: "./scripts/blue_user_data.sh"
  green_user_data_file: "./scripts/green_user_data.sh"
  blue_instance_type: "t3.micro"
  green_instance_type: "t3.micro"
  traffic_distribution: "green"
  vpc_id: "vpc-xxxxx"
  alb_subnets_ids: ["subnet-xxxxx", "subnet-xxxxx"]
  tags:
    Name: example-canary-asg
    Description: example-canary-asg
```

```yaml
provider "aws" {
  profile = local.manifest.project.profile
  region  = local.manifest.project.region
}

locals {
  manifest = yamldecode(file("${path.cwd}/manifest.yaml"))
}

module "module_usage_howto" {
  source    = "./../.."
  project   = local.manifest.project
  resources = local.manifest.resources
}
```

## Example from code .tf
```yaml
provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

module "module_usage_howto" {
  source = "./../.."

  project = {
    project     = "my-project"
    environment = "dev"
    createdBy   = "markitos"
    group       = "my-group"
  }

  resources = {
    name                         = "markitos_asg"
    lb_access_log_enabled        = true
    lb_connection_log_enabled    = true
    lb_delete_protection_enabled = false
    asg_desired_capacity         = 2
    asg_min_size                 = 1
    asg_max_size                 = 2
    blue_ami                     = "ami-0c38b837cd80f13bb"
    green_ami                    = "ami-0c38b837cd80f13bb"
    blue_user_data_file          = "${path.cwd}/scripts/blue_user_data.sh"
    green_user_data_file         = "${path.cwd}/scripts/green_user_data.sh"
    blue_instance_type           = "t3.micro"
    green_instance_type          = "t3.micro"
    traffic_distribution         = "split"
    vpc_id                       = "vpc-784a8a1d"
    alb_subnets_ids              = toset(["subnet-4eaf5c17", "subnet-ef815e98"])
    tags = {
      Name = "basic-canary-asg"
    }
  }
}
```
<!-- END_TF_DOCS -->